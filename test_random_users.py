from randoms import *
from random import randint

def test_random_users():
    '''Manual test random user.

    Sqlite functions can be wrapped into a class to reduce code duplicate and improve code reusability.
    Keep in duplicated structure for easy reading.
    '''
    conn = None
    table = 'user'
    db_path = 'random_user_db.sqlite3'
    drop_query = 'DROP TABLE IF EXISTS {tn}'.format(tn=table)
    create_query = 'CREATE TABLE IF NOT EXISTS {tn} (id integer PRIMARY KEY, name text, email text, password text)'.format(tn=table)

    # initail a database and table
    try:
        conn = sqlite3.connect(db_path)
        cur = conn.cursor()
        cur.execute(drop_query)
        cur.execute(create_query)
        conn.commit()
    except sqlite3.Error as e:
        print("Database error: %s" % e)
    except Exception as e:
        print("Exception in query: %s" % e)
    finally:
        if conn:
            conn.close()

    # generate 10 users
    for _ in range(10):
        create_user(db_path)

    # add passwords
    update_query = 'UPDATE {tn} SET password = ? WHERE id = ?'.format(tn=table)
    for i in range(1, 11):
        try:
            conn = sqlite3.connect(db_path)
            cur = conn.cursor()
            cur.execute(update_query, (generate_password(randint(6,12), randint(1, 4)), i))
            conn.commit()
        except sqlite3.Error as e:
            print("Database error: %s" % e)
        except Exception as e:
            print("Exception in query: %s" % e)
        finally:
            if conn:
                conn.close()

    # print results
    select_query = 'SELECT * FROM {tn}'.format(tn=table)
    try:
        conn = sqlite3.connect(db_path)
        cur = conn.cursor()
        cur.execute(select_query)
        data = cur.fetchall()
        print(data)
    except sqlite3.Error as e:
        print("Database error: %s" % e)
    except Exception as e:
        print("Exception in query: %s" % e)
    finally:
        if conn:
            conn.close()

if __name__ == '__main__':
    test_random_users()