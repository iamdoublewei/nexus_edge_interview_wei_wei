# Solution Documentation
Video Link: https://drive.google.com/open?id=13ABBiho0eTB9tEZLsC0DwYdmeR8-Ibys

randoms.py
1. generate_password
    * Initial a pool to store all the possible character for a certain input.
    * Initial a password list to store all the characters of a password.
    * Check if the given complexity is satisfied each complexity level in ascending order.
    * If satisfied, add additional characters before previous level into the pool.
    * And also if complexity > 1, add minimum complexity character requirement into password list.
    * After checking all complexity levels, add additional rondom characters (length - minimum characters) from the pool into password list.
    * Reorder the password list and join all charcters in the password list into a password and return the password.
    * Since the requirement does not specify corner cases inputs, such as length <= 0 and length cannot fullfill the complexity level. So in this code, I assume all the inputs are valid.

2. check_password_level
    * Go through each character in a password, check if the character is a digit/uppercase/punctuation.
    * If the password has digit, uppercase and punctuation, then complexity = 4.
    * If the password has digit and uppercase but no punctuation, then complexity = 3.
    * If the password has digit, but no uppercase and punctuation, then complexity = 2.
    * If the password has no digit, uppercase and punctuation, then complexity = 1.
    * Now we have a complexity for a password. Do a override second check for the password.
    * If length of the password >= 8 and has no digit, uppercase and punctuation, then complexity = 2.
    * If length of the password >= 8 and has digit, but no uppercase and punctuation, then complexity = 3.
    * Return the final complexity.
    
3. A practical Python test
    * Pull random user name and email from API into json format.
    * Find name and email address from json string.
    * Store user info into provided sqlite database file.
    * Assume the sqlite database file exists and the required table exists.
    
test_random_users.py
1. Test getting random users from API. Create rondom password for generated users.

test_random_password.py
1. Automated unit test for generating random password.

requirement.txt
1. Additional third party libary required.

For the video and reading.
1. I am impressed your guys having a great parnership with Oracle. 
2. The flawless ALANA could be the key success of the company. 
3. Just having a brainstorming. Is that possible to have ALANA work with Voice Control System?