import unittest
from randoms import *

class TestRandomPasswords(unittest.TestCase):
    '''Automated unit test for random passwords'''

    def test_random_passwords(self):
        '''Unit test random password creation.

        Since the question requirement does not specify corner cases for the inputs.
        Assume all the inputs are valid.

        Corner cases:
        1. password length <= 0
        2. password length is not long enough to generate the input complexity level.
        '''
        # test length < 8
        p = generate_password(7 ,1)
        self.assertEqual(check_password_level(p), 1, msg='length < 8, complexity 1 failed.')
        p = generate_password(7 ,2)
        self.assertEqual(check_password_level(p), 2, msg='length < 8, complexity 2 failed.')
        p = generate_password(7 ,3)
        self.assertEqual(check_password_level(p), 3, msg='length < 8, complexity 3 failed.')
        p = generate_password(7 ,4)
        self.assertEqual(check_password_level(p), 4, msg='length < 8, complexity 4 failed.')
        # test length = 8
        p = generate_password(8 ,1)
        self.assertEqual(check_password_level(p), 2, msg='length = 8, complexity 1 failed.')
        p = generate_password(8 ,2)
        self.assertEqual(check_password_level(p), 3, msg='length = 8, complexity 2 failed.')
        p = generate_password(8 ,3)
        self.assertEqual(check_password_level(p), 3, msg='length = 8, complexity 3 failed.')
        p = generate_password(8 ,4)
        self.assertEqual(check_password_level(p), 4, msg='length = 8, complexity 4 failed.')
        # test length > 8
        p = generate_password(9 ,1)
        self.assertEqual(check_password_level(p), 2, msg='length > 8, complexity 1 failed.')
        p = generate_password(10 ,2)
        self.assertEqual(check_password_level(p), 3, msg='length > 8, complexity 2 failed.')
        p = generate_password(11 ,3)
        self.assertEqual(check_password_level(p), 3, msg='length > 8, complexity 3 failed.')
        p = generate_password(12 ,4)
        self.assertEqual(check_password_level(p), 4, msg='length > 8, complexity 4 failed.')

if __name__ == '__main__':
    unittest.main()