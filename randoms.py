# DO NOT USE THIS FILE. YOU MAY COPY ITS CONTENT INTO ANOTHER SCRIPT.

"""
    There are a few libs imported here, their use is only optional
    you may import other libs as well but you need to provide
    a requirements.txt if you use any third-party libs.
"""

import random  # https://docs.python.org/3.6/library/random.html
import sqlite3  # https://docs.python.org/3.6/library/sqlite3.html
import string  # https://docs.python.org/3.6/library/string.html
import requests # http://docs.python-requests.org/en/master/

def generate_password(length: int, complexity: int) -> str:
    """Generate a random password with given length and complexity

    Since this question requirement does not specify corner cases for the inputs.
    Assume all the inputs are valid.

    Corner cases:
    1. password length <= 0
    2. password length is not long enough to generate the input complexity level.

    Complexity levels:
        Complexity == 1: return a password with only lowercase chars
        Complexity ==  2: Previous level plus at least 1 digit
        Complexity ==  3: Previous levels plus at least 1 uppercase char
        Complexity ==  4: Previous levels plus at least 1 punctuation char

    :param length: number of characters
    :param complexity: complexity level
    :returns: generated password
    """
    chars, password = '', []

    if complexity >= 1:
        chars += string.ascii_lowercase
    if complexity >= 2:
        chars += string.digits
        password.append(random.choice(string.digits))
        length -= 1
    if complexity >= 3:
        chars += string.ascii_uppercase
        password.append(random.choice(string.ascii_uppercase))
        length -= 1
    if complexity >= 4:
        chars += string.punctuation
        password.append(random.choice(string.punctuation))
        length -= 1
    # corner case: the length cannot fullfill complexity
    if length < 0:
        return ''
    password += random.choices(chars, k=length)
    random.shuffle(password)

    return ''.join(password)

def check_password_level(password: str) -> int:
    """Return the password complexity level for a given password

    Complexity levels:
        Return complexity 1: If password has only lowercase chars
        Return complexity 2: Previous level condition and at least 1 digit
        Return complexity 3: Previous levels condition and at least 1 uppercase char
        Return complexity 4: Previous levels condition and at least 1 punctuation

    Complexity level exceptions (override previous results):
        Return complexity 2: password has length >= 8 chars and only lowercase chars
        Return complexity 3: password has length >= 8 chars and only lowercase and digits

    :param password: password
    :returns: complexity level
    """
    if not password: return 0

    complexity, isDigit, isUpper, isPunc = 1, False, False, False

    # analyze the given password
    for char in password:
        if char.isupper():
            isUpper = True
        elif char.isdigit():
            isDigit = True
        elif char in string.punctuation:
            isPunc = True

    # complexity levels
    if isDigit and isUpper and isPunc:
        complexity = 4
    elif isDigit and isUpper:
        complexity = 3
    elif isDigit:
        complexity = 2

    # complexity level exceptions
    if len(password) >= 8 and complexity in [1, 2]:
        complexity += 1

    return complexity

def create_user(db_path: str) -> None:  # you may want to use: http://docs.python-requests.org/en/master/
    """Retrieve a random user from https://randomuser.me/api/
    and persist the user (full name and email) into the given SQLite db

    :param db_path: path of the SQLite db file (to do: sqlite3.connect(db_path))
    :return: None
    """
    table = 'user'
    url = 'https://randomuser.me/api/'
    fullname, email = '', ''

    # get random user from api
    try:
        response = requests.get(url)
        fullname = response.json()['results'][0]['name']['first'] + ' ' + response.json()['results'][0]['name']['last']
        email = response.json()['results'][0]['email']
    except requests.exceptions.RequestException as e:  # This is the correct syntax
        print(e)

    # persist random user info into db
    if fullname and email:
        conn = None
        insert_query = 'INSERT INTO {tn} (name, email) VALUES (?,?)'.format(tn=table)

        try:
            conn = sqlite3.connect(db_path)
            cur = conn.cursor()
            cur.execute(insert_query, (fullname, email))
            conn.commit()
        except sqlite3.Error as e:
            print("Database error: %s" % e)
        except Exception as e:
            print("Exception in query: %s" % e)
        finally:
            if conn:
                conn.close()